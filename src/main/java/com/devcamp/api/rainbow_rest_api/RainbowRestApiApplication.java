package com.devcamp.api.rainbow_rest_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowRestApiApplication.class, args);
	}

}
